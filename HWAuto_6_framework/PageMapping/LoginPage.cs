﻿using Core;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;

namespace PageMapping
{
	public class LoginPage : BasePage
	{
		public LoginPage(IWebDriverExt<IWebDriver> driver) : base(driver)
		{
		}

		protected override bool EvaluateLoadedStatus()
		{
			return WaitHelper.WaitUntilElementIsDisplayed(CitiesBlock, WaitTimeout.Small);
		}

		public CommonElement CitiesBlock => new CommonElement(new CssLocator("#weather-cities"), DefaultParent);

		public InputField SearchField => new InputField(new IdLocator("ya"), DefaultParent);

		public CommonElement Submit => new CommonElement(new CssLocator("#search-city [type='submit']"), DefaultParent);

	}
}