﻿using Core;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;

namespace PageMapping
{
    public class CitiesPage : BasePage    
    {

        public CitiesPage(IWebDriverExt<IWebDriver> driver) : base(driver)  
        {
        }

        //Find element Kharkiv
        public Label Kharkiv => new Label(new XpathLocator("//a[@href='/weather-kharkiv-5053/']//span"), DefaultParent);

    }
}
