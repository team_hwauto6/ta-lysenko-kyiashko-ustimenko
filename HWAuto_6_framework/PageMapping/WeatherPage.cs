﻿using Core;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Core.Extentions;
using System;
using System.Linq;


namespace PageMapping
{
    public class WeatherPage : BasePage
    {
        public WeatherPage(IWebDriverExt<IWebDriver> driver) : base(driver)
        {
        }



        // 2 weeks forecast element
        public Label TwoWeeksForecast => new Label(new CssLocator("a[href*='14-days/']:first-of-type"), DefaultParent);
        public Label TabOpener => new Label(new CssLocator("#tabs_opener"), DefaultParent);
        public Label TabCloser => new Label(new CssLocator("#tabs_closer"), DefaultParent);        
        
        public IReadOnlyCollection<IWebElement> TwoWeeks
        {
            get
            {
                return IoCResolver.WebDriver.WebDriver.FindElements(By.CssSelector("div.s_date"));
            }
        }
        
        
        public void DateAssertion(IReadOnlyCollection<IWebElement> listOfDays, DateTime today)
        {
            for (int i = 0; i <  listOfDays.Count; i++)
            {
                try
                {
                    string day = DateTime.Parse(listOfDays.ElementAt(i).Text).ToString("dd-MM-yyyy");
                    string localtoday = today.AddDays(i).ToString("dd-MM-yyyy");
                    if (day != localtoday)                     
                    {
                        throw new ArgumentException("A day from list doesn't match with localday", day);
                    }
                }
                catch(FormatException)
                {
                }
                              
            }
        }

        public CommonElement TomorowDate => new CommonElement(new XpathLocator("//div[@id='tab_wdaily2']/div[@class='temp']/em"), DefaultParent);
        public Label NextThreeDays => new Label(new XpathLocator("//a[@href='/weather-kharkiv-5053/3-5-days/' and @class='next']"), DefaultParent);
        public CommonElement NextFromTwoWeeks => new CommonElement(new XpathLocator("(//td[@class='temp'])[23]"), DefaultParent);
    
    }
}
