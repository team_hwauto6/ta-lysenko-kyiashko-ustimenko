﻿using System;
using Core;
using PageMapping;

namespace SeleniumTests
{
	public class BaseTest : TestInstanceBase
	{
		protected Lazy<LoginPage> LoginPageLazy;
		protected Lazy<WeatherPage> WeatherPageLazy;
        protected Lazy<CitiesPage> CitiesPageLazy;

        protected override void InitPages()
		{
			LoginPageLazy = new Lazy<LoginPage>(() => new LoginPage(Browser));
            WeatherPageLazy = new Lazy<WeatherPage>(() => new WeatherPage(Browser));
            CitiesPageLazy = new Lazy<CitiesPage>(() => new CitiesPage(Browser));
        }

		public LoginPage LoginPage => LoginPageLazy.Value.Load() as LoginPage;
		public WeatherPage WeatherPage => WeatherPageLazy.Value.Load() as WeatherPage;
        public CitiesPage CitiesPage => CitiesPageLazy.Value.Load() as CitiesPage;


    }
}
