﻿using NUnit.Framework;
using PageMapping;
using System;
using System.Linq;

namespace SeleniumTests
{
    [TestFixture]
    public class Ustimenko : BaseTest
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            OpenBrowser();
        }

        [Test]
        public void TwoWeeksAreMatchesWithPeriod()
        {
            FollowLink("http://gismeteo.ua");
            LoginPage.SearchField.Type("Харьков");
            LoginPage.Submit.Click();
            CitiesPage.Kharkiv.Click();
            WeatherPage.TwoWeeksForecast.Click();
            WeatherPage.TabOpener.Click();
            WeatherPage.TabCloser.Click();
            var twoWeeksList = WeatherPage.TwoWeeks;
            DateTime localDate = DateTime.Now;
            DateTime localToday = DateTime.Parse(localDate.ToString());
            //try
            //{
            //    DateTime firstDay = DateTime.Parse(twoWeeksList.ElementAt(0).Text);
            //    DateTime localLastDay = localDate.AddDays(13);
            //    DateTime lastDay = DateTime.Parse(twoWeeksList.ElementAt(13).Text);
            //    Assert.AreEqual(localToday.ToString("dd-MM-yyyy"), firstDay.ToString("dd-MM-yyyy"));
            //    Assert.AreEqual(localLastDay.ToString("dd-MM-yyyy"), lastDay.ToString("dd-MM-yyyy"));
            //}
            //catch (FormatException)
            //{
            //}
            WeatherPage.DateAssertion(twoWeeksList, localDate);
        }


    }
}
