﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using PageMapping;

namespace SeleniumTests
{
    [TestFixture]
    public class KyiashkoTest : BaseTest
    {
        [OneTimeSetUp]
        public void Setup()
        {
            OpenBrowser();
        }
        [Test]
        public void DayTempMatcheTwoWeekTemp()
        {
            FollowLink("http://gismeteo.ua");
            LoginPage.SearchField.Type("Харьков");
            LoginPage.Submit.Click();
            CitiesPage.Kharkiv.Click();
            WeatherPage.NextThreeDays.Click();
            string NextThreeDayTemp = WeatherPage.TomorowDate.InnerText;
            WeatherPage.TwoWeeksForecast.Click();
            WeatherPage.TabOpener.Click();
            string NextTwoWeekTemp = WeatherPage.NextFromTwoWeeks.InnerText;
            Assert.AreEqual(NextThreeDayTemp, NextTwoWeekTemp, "The temperature should be the same");
        }
    }
}

