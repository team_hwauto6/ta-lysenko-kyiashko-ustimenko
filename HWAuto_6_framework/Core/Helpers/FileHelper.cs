﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Core.Helpers
{
	public class FileHelper
	{
		public static void SerializeToXml<T>(T entity, string fullFileName)
		{
			var serializer = new XmlSerializer(typeof(T));

			using (var writer = new FileStream(fullFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
			{
				serializer.Serialize(writer, entity);
			}
		}

		public static T DeserializeFromXml<T>(string fullFileName)
		{
			var serializer = new XmlSerializer(typeof(T));

			using (var reader = new FileStream(fullFileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
			{
				var entity = serializer.Deserialize(reader);
				return (T)entity;
			}
		}

		public static void WriteEnumerableToFile<T>(IEnumerable<T> entities, string fullPath)
		{
			WriteToFile(entities, fullPath, (x, y) =>
			{
				foreach (var entity in x)
				{
					y.WriteLine(entity.ToString());
				}
			});
		}

		public static void WriteEntityToFile<T>(T obj, string filePath, Func<T, string> entityToString = null)
		{
			WriteToFile(obj, filePath, (x, y) => y.Write(entityToString == null ? obj.ToString() : entityToString(obj)));
		}

		private static void WriteToFile<T>(T entities, string fullPath, Action<T, StreamWriter> writeAction)
		{
			using (var fileWriter = new StreamWriter(new FileStream(fullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)))
			{
				writeAction(entities, fileWriter);
			}
		}

		public static string ReadFromFile(string filePath)
		{
			return ReadFromFile(filePath, x => x.ReadToEnd());
		}

		public static IEnumerable<string> ReadEnumerableFromFile(string filePath, bool fileContainsHeader = true)
		{
			return ReadFromFile(filePath, x =>
			{
				var result = new List<string>();
				if (fileContainsHeader) x.ReadLine();
				while (!x.EndOfStream)
				{
					var row = x.ReadLine();
					if (row == null) return result;
					result.Add(row);
				}
				return result;
			});
		}

		private static T ReadFromFile<T>(string filePath, Func<StreamReader, T> readFunc)
		{
			using (var fileReader = new StreamReader(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
			{
				return readFunc(fileReader);
			}
		}

		public static Dictionary<string, string> ReadFromCsvToDictionary(string pathToFile)
		{
			return File.ReadLines(pathToFile)
				.Select(line => line.Split(','))
				.ToDictionary(line => line[0], line => line[1]);
		}

		public static void EnsureDirectoryExists(string dirPath)
		{
			var logDirInfo = new DirectoryInfo(dirPath);

			if (!logDirInfo.Exists)
			{
				logDirInfo.Create();
			}
		}

		public static bool IsFileExists(string dirPath)
		{
			var fileInfo = new FileInfo(dirPath);

			return fileInfo.Exists;
		}

		public static void CleanUpDirectory(string dirPath)
		{
			var logDirInfo = new DirectoryInfo(dirPath);

			if (logDirInfo.Exists)
			{
				logDirInfo.Delete(true);
			}
		}
	}
}