﻿namespace Core.Enums
{
	public enum TestLogLevel
	{
		Debug,
		Info,
		Warn,
		Error,
		Fatal,
		Step
	}
}
