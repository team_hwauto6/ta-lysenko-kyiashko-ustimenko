﻿using NLog.LayoutRenderers;

namespace Core.TestLogging
{
	[LayoutRenderer("StepInfo")]
	public class StepInfoLayoutRenderer : BaseLayoutRenderer
	{
		protected override string PropertyName => "StepInfo";
	}
}
