﻿using NLog.LayoutRenderers;

namespace Core.TestLogging
{
	[LayoutRenderer("TestName")]
	public class TestNameLayoutRenderer : BaseLayoutRenderer
	{
		protected override string PropertyName => "TestName";
	}
}
