﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using Core.Helpers;
using Core.Interfaces;
using Core.TestLogging;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace Core.Extentions
{
	public class WebDriverExt<T> : IWebDriverExt<T>
		where T : IWebDriver
	{
		public T WebDriver { get; set; }

		private TestLogger TestLogger = IoCResolver.TestLogger;

		private const int PageRenderingTime = 250;
		private const int _pageDefaultZoomFactor = 100;
		private const int PageReZoomTime = 250;
		private const int ScrollWaitTime = 300;
		private const int VisibilityManipulationWaitTime = 200;

		private const string InternalViewDefaultFrameIdentifier = @"class=""ng-scope""";

		public WebDriverExt(T driver)
		{
			WebDriver = driver;
		}

		public bool IsOnAgentViewDefaultFrame
		{
			get
			{
				string pageSource;

				try
				{
					pageSource = WebDriver.PageSource;
				}
				catch (WebDriverException)
				{
					//try again
					IoCResolver.TestLogger.LogWarning("Cannot obtain page source during default selenium timeout");
					pageSource = WebDriver.PageSource;
				}
				return pageSource.Contains(InternalViewDefaultFrameIdentifier);
			}
		}

		public int PageDefaultZoomFactor => _pageDefaultZoomFactor;

		public void ShutDown()
		{
			if (WebDriver == null) return;
			try
			{
				WebDriver.Quit();
				WebDriver.Dispose();
				IoCResolver.WebDriver = null;
			}
			catch (Exception exception)
			{
				TestLogger.LogWarning("Shutdown WebDriver failed", null, exception);
			}
		}

		public void FollowLink(string urlString, int attempts)
		{
			WaitHelper.TryAction(() => WebDriver.Navigate().GoToUrl(urlString), attempts);
		}

		/// <summary>
		/// Use for basic http authentication
		/// </summary>
		/// <param name="urlString">Environment url</param>
		/// <param name="login">User login</param>
		/// <param name="password">User password</param>
		/// <param name="attempts"></param>
		public void FollowLink(string urlString, string login, string password, int attempts)
		{
			var updatedUrl = urlString.Replace("://", $"://{login}:{password}@");
			WaitHelper.TryAction(() => WebDriver.Navigate().GoToUrl(updatedUrl), attempts);
		}

		/// <summary>
		/// This method generate one url from list of urls
		/// </summary>
		/// <param name="urls">Urls list</param>
		public void FollowLink(params string[] urls)
		{
			WebDriver.Navigate().GoToUrl(String.Join("", urls));
		}

		public void Maximize()
		{
			WebDriver.Manage().Window.Maximize();
		}

		public TY ExecuteScript<TY>(string scriptStr, bool log = false, params object[] args)
		{
			var logStepInfo = $"{MethodBase.GetCurrentMethod().Name}: {scriptStr}";

			var scriptExecutor = WebDriver as IJavaScriptExecutor;
			if (scriptExecutor == null)
			{
				if (log)
				{
					TestLogger.LogError(logStepInfo, null, new Exception("'IWebDriver as IJavaScriptExecutor' returned null"));
				}
				return default(TY);
			}

			try
			{
				var scriptResult = scriptExecutor.ExecuteScript(scriptStr, args);
				if (log)
				{
					TestLogger.LogInfo(logStepInfo);
				}

				return (TY)scriptResult;
			}
			catch (Exception e)
			{
				if (log)
				{
					TestLogger.LogError(logStepInfo, null, e);
				}
				return default(TY);
			}
		}

		/// <summary>
		/// Wait for DOM to be ready and page to be rendered(doesn't work on internal's default view)
		/// </summary>
		public void WaitForPageLoading()
		{
			if (IsOnAgentViewDefaultFrame)
				return;

			var methodName = MethodBase.GetCurrentMethod().Name;
			var startWaitTime = DateTime.Now;

			WebDriver.WaitForCondition(d => d.ExecuteScript<bool>("return app.core.isReady()", false),
				IoCResolver.Settings.WaitDomLoadingTimeout, logNotes: methodName);

			var waitTimeStr = (DateTime.Now - startWaitTime).Seconds;
			var logStepInfo = $"{methodName} had been executed for {waitTimeStr} sec";
			TestLogger.LogInfo(logStepInfo);

			//Wait for page rendering as DOM being ready doesn't mean rendered page
			Thread.Sleep(PageRenderingTime);
		}

		/// <summary>
		/// Switches to any inner iframe 'Document' scope by specified element or to default scope on the page.
		/// </summary>
		/// <param name="frameElement">iFrame element to switch context to. If null, then switches to default page content</param>
		/// <returns>webDriver referenced to new scope/page context</returns>
		public T SwitchContextTo(IBaseElement frameElement)
		{
			if (frameElement == null)
			{
				TestLogger.LogInfo("Frame element is null");
				return WebDriver;
			}
			WebDriver = SwitchToDefaultScope();

			var stepInfo =
				$"Execute {MethodBase.GetCurrentMethod().Name} on element {frameElement.GetType().Name} by {frameElement.Locator.GetType().Name} = '{frameElement.Locator.Value}'.";

			if (frameElement.Object == null)
			{
				TestLogger.LogWarning(stepInfo, null, new Exception("Referenced WebElement is null"));
				return WebDriver;
			}

			try
			{
				WebDriver = (T)WebDriver.SwitchTo().Frame(frameElement.Object);
			}
			catch (Exception exception)
			{
				TestLogger.LogWarning(stepInfo, null, exception);
			}

			return WebDriver;
		}

		public T SwitchToDefaultScope()
		{
			return (T)WebDriver.SwitchTo().DefaultContent();
		}

		public string TakeScreenshot(string pathToSave = null, string fileName = null)
		{
			var takeScreen = WebDriver as ITakesScreenshot;
			if (takeScreen == null)
			{
				TestLogger.LogWarning("Failed to cast WebDriver to ITakesScreenshot");
				return string.Empty;
			}

			if (fileName == null)
			{
				fileName = TestContext.CurrentContext.Test.GetTestName() + "_" + StampString(DateTime.Now);
			}
			string regex = $@"[{Regex.Escape(new string(Path.GetInvalidFileNameChars()))}]+";
			fileName = Regex.Replace(fileName, regex, "_");

			pathToSave = pathToSave ?? PathHelper.ScreensCurrent;

			string screenName;
			int i = 1;
			do
			{
				screenName = PathHelper.CombineFullPath(fileName + (i == 1 ? "" : $" - {i}"), PathHelper.Ext.Jpg, pathToSave);
			} while (File.Exists(screenName));

			int totalHeight = TakeScreenshotHelper.TotalHeight ?? 0;
			int viewPortHeight = TakeScreenshotHelper.ViewportHeight ?? 0;

			if (totalHeight == 0 || viewPortHeight == 0)
				return string.Empty;

			var zoomFactor = PageDefaultZoomFactor * viewPortHeight / totalHeight;

			//if (zoomFactor >= 50)
			return TakeScreenshotHelper.TakeSingleScreenshot(takeScreen, zoomFactor, screenName);
			//return TakeScreenshotHelper.StitchMultipleScreenshotsToOne(totalHeight, viewPortHeight, screenName);
		}

		public TY WaitUntil<TY>(Func<IWebDriver, TY> exitWaitCondition, int maxInterval = 120, string callerMethodName = null)
		{
			var stepLogInfo =
				$"Execute {callerMethodName ?? MethodBase.GetCurrentMethod().Name} on {(WebDriver != null ? WebDriver.GetType().Name : "element")}";

			if (WebDriver == null) return default(TY);

			var driverWait = new WebDriverWait(WebDriver, new TimeSpan(0, 0, maxInterval));
			try
			{
				var waitOutput = driverWait.Until(exitWaitCondition);
				TestLogger.LogInfo(stepLogInfo);
				return waitOutput;
			}
			catch (Exception exception)
			{
				TestLogger.LogWarning(stepLogInfo, null, exception);
				return default(TY);
			}
		}

		public void RefreshPage()
		{
			WebDriver.Navigate().Refresh();
			WebDriver.WaitForPageLoading();
		}

		public Uri GetCurrentUrl()
		{
			TimeSpan maxDuration = TimeSpan.FromSeconds(IoCResolver.Settings.UrlLoadingTimeout);
			Stopwatch sw = Stopwatch.StartNew();

			while (sw.Elapsed < maxDuration)
			{
				try
				{
					return new Uri(WebDriver.Url);
				}
				catch (Exception e)
				{
					TestLogger.LogWarning("Getting WebDriver url was not successful", null, e);
				}
			}
			TestLogger.LogError("Getting WebDriver url was not successful", null, new TimeoutException("Unable to get web driver url within " + maxDuration.Seconds + " seconds timeout"));
			return null;
		}

		public void ClickElementByOffset(IBaseElement elementToClick, int offsetX, int offsetY, int additionalOffset = -200)
		{
			IoCResolver.TestLogger.LogInfo($"Click on {elementToClick.Locator.By} menu by Selenium Action with offset x: {offsetX}, y:{offsetY}");
			//Note that additional offset 200 is used to have the element visible under top submenu
			ScrollToElement(elementToClick, additionalOffset);
			var webElement = WebDriver.GetWebElement(elementToClick.Locator);
			var builder = new Actions(WebDriver);
			builder.MoveToElement(webElement).MoveByOffset(offsetX, offsetY).Click().Build().Perform();
			WebDriver.WaitForPageLoading();
		}

		public void ClickElementByJsExecutor(IBaseElement element)
		{
			if (!WebDriver.IsElementPresent(element.Locator)) return;

			var webElement = WebDriver.GetWebElement(element.Locator);
			TestLogger.LogInfo("Click with JS executor on the element with locator " + element.Locator.By);
			((IJavaScriptExecutor)WebDriver).ExecuteScript("arguments[0].click();", webElement);
			WebDriver.WaitForPageLoading();
		}

		public bool ScrollToElement(IBaseElement elementToScroll, int additionalOffset = 0)
		{
			var position = WebDriver.FindElement(elementToScroll.Locator.By).Location.Y + additionalOffset;
			ScrollBy(0, position);
			return elementToScroll.IsDisplayed;
		}

		public void ScrollBy(int x, int y)
		{
			((IJavaScriptExecutor)WebDriver).ExecuteScript($"window.scrollBy({x}, {y});");
			Thread.Sleep(ScrollWaitTime);
		}

		public void ZoomBy(int zoomFactor)
		{
			try
			{
				((IJavaScriptExecutor)WebDriver).ExecuteScript($"document.body.style.zoom = '{zoomFactor}%';");
			}
			catch (Exception)
			{
				IoCResolver.TestLogger.LogWarning("Failed to zoom window");
			}
			finally
			{
				((IJavaScriptExecutor)WebDriver).ExecuteScript($"document.body.style.zoom = '{zoomFactor}%';");
			}
			Thread.Sleep(PageReZoomTime);
		}

		public void ZoomToDefault()
		{
			ZoomBy(PageDefaultZoomFactor);
		}

		public void MakeHidden(IWebElement element)
		{
			((IJavaScriptExecutor)WebDriver).ExecuteScript("arguments[0].style.visibility='hidden'", element);
			Thread.Sleep(VisibilityManipulationWaitTime);
		}

		public void MakeVisible(IWebElement element)
		{
			((IJavaScriptExecutor)WebDriver).ExecuteScript("arguments[0].style.visibility='visible'", element);
			Thread.Sleep(VisibilityManipulationWaitTime);
		}

		public void ClearSessionData()
		{
			WebDriver.Manage().Cookies.DeleteAllCookies();
			WebDriver.ExecuteJavaScript<string>("window.localStorage.clear();");
			WebDriver.ExecuteJavaScript<string>("window.sessionStorage.clear();");
		}

		public void MoveToElement(IBaseElement element)
		{
			var builder = new Actions(WebDriver);
			builder.MoveToElement(WebDriver.FindElement(element.Locator.By)).Build().Perform();
		}

		private string StampString(DateTime dateTime)
		{
			return dateTime.ToString("yyyy-MM-dd_HH-mm-ss_fff");
		}
	}
}
