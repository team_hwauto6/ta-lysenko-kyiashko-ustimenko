﻿using System;
using Core.TestData.Models;

namespace Core.TestData.Settings
{
	[Serializable]
	public class AddressSettings
	{
		public Address TestAddress1 { get; set; }
	}
}
