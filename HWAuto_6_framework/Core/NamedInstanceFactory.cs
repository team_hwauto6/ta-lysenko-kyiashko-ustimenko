﻿using System;
using Core.Interfaces;
using StructureMap;

namespace Core
{
	public class NamedInstanceFactory<T> : INamedInstanceFactory<T>
		where T : class
	{
		private readonly IContainer _container;

		public NamedInstanceFactory(IContainer container)
		{
			_container = container;
		}

		public T Get(string name)
		{
			return _container.GetInstance<T>(name);
		}

		public T Get(Enum name)
		{
			return Get(name.ToString());
		}

		public T TryGet(string name)
		{
			return _container.TryGetInstance<T>(name);
		}

		public T TryGet(Enum name)
		{
			return TryGet(name.ToString());
		}
	}
}
