﻿using Core.UIControls.Locators;
using OpenQA.Selenium;

namespace Core.Interfaces
{
	public interface IBaseElement
	{
		ISearchContext Parent { get; }
		ILocator Locator { get; }
		IWebElement Object { get; }
		bool IsPresent { get; }
		bool IsDisplayed { get; }
		bool IsClickable { get; }
		string InnerText { get; }
		string Value { get; }
		void Click(bool throwException = true);
		string GetAttribute(string attrName);
	}
}