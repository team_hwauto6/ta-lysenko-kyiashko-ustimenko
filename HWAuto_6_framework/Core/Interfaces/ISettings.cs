﻿namespace Core.Interfaces
{
	public interface ISettings
	{
		string WebDriver { get; }
		bool TakeErrorScreens { get; }
		bool CleanUpLogDir { get; }
		string LogSeparator { get; }
		int WaitDomLoadingTimeout { get; }
		int UrlLoadingTimeout { get; }
	}
}