﻿using System;
using Core.Enums;

namespace Core.Interfaces
{
	public interface ILogger
	{
		void RegisterLoggingRuleByTargetName(string name);

		void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo);

		void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo, string screenshotPath);

		void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo, string screenshotPath, string message);

		void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo,  string screenshotPath, Exception ex);
	}
}