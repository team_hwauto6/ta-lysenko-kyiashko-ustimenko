﻿using System;
using OpenQA.Selenium;

namespace Core.Interfaces
{
	public interface IWebDriverExt<T>
		where T : IWebDriver
	{
		T WebDriver { get; set; }
		bool IsOnAgentViewDefaultFrame { get; }
		int PageDefaultZoomFactor { get; }

		void ShutDown();
		void FollowLink(string urlString, int attempts);
		void FollowLink(string urlString, string login, string password, int attempts);
		void FollowLink(params string[] urls);
		void Maximize();
		TY ExecuteScript<TY>(string scriptStr, bool log = false, params object[] args);
		void WaitForPageLoading();
		T SwitchContextTo(IBaseElement frameElement);
		T SwitchToDefaultScope();
		string TakeScreenshot(string pathToSave = null, string fileName = null);
		TY WaitUntil<TY>(Func<IWebDriver, TY> exitWaitCondition, int maxInterval = 120, string callerMethodName = null);
		void RefreshPage();
		Uri GetCurrentUrl();
		void ClickElementByOffset(IBaseElement elementToClick, int offsetX, int offsetY, int additionalOffset = -200);
		void ClickElementByJsExecutor(IBaseElement element);
		bool ScrollToElement(IBaseElement elementToScroll, int additionalOffset = 0);
		void ScrollBy(int x, int y);
		void ZoomBy(int zoomFactor);
		void ZoomToDefault();
		void MakeHidden(IWebElement element);
		void MakeVisible(IWebElement element);
		void ClearSessionData();
		void MoveToElement(IBaseElement element);
	}
}
