﻿using System;
using System.Diagnostics;
using Core.Interfaces;
using Core.UIControls.Locators;
using OpenQA.Selenium;

namespace Core.UIControls.SimpleControls
{
	public class Label : CommonElement
	{
		public Label(ILocator locator, ISearchContext parent, int? timeout = null, string expectedText = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			ExpectedText = expectedText;
		}

		public Label(ILocator locator, IBaseElement parent, int? timeout = null, string expectedText = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			ExpectedText = expectedText;
		}

		public string ExpectedText { get; private set; }

		public bool HasRelevantText => !string.IsNullOrEmpty(ExpectedText) && InnerText.Contains(ExpectedText);
	}
}
