﻿﻿using System;
using System.Collections.Generic;
﻿using System.Diagnostics;
﻿using System.Linq;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Extensions;
﻿using Telenor.OneScreen.UITests.Core.Helpers;
﻿using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	/// <summary>
	/// To be used only for non-OneScreen related pages(e.g. Nets page) as OneScreen pages have wrappers for web elements disabling manipulation with standard Webdriver facilities
	/// </summary>
	public class DropDownJs : CommonElement
	{
		private Lazy<IEnumerable<string>> _options;
		private Lazy<string> _selected;
		private string _selector;

		/// <summary>
		/// Custom element. Implements main drop-down api via WebDriver execute script.
		/// </summary>
		/// <param name="locator">Allowed Id or Css locators.</param>
		/// <param name="parent">DOM context to find element within.</param>
		/// <param name="timeout">Non default timeout for element wait</param>
		/// <param name="waitCondition">Non default condition for element wait</param>
		public DropDownJs(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			InitFields();
		}

		/// <summary>
		/// Custom element. Implements main drop-down api via WebDriver execute script.
		/// </summary>
		/// <param name="locator">Allowed Id or Css locators.</param>
		/// <param name="parent">DOM context to find element within.</param>
		/// <param name="timeout">Non default timeout for element wait</param>
		/// <param name="waitCondition">Non default condition for element wait</param>
		public DropDownJs(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			InitFields();
		}

		protected void InitFields()
		{
			_selector = string.Format("{0}{1}", Locator is IdLocator ? "#" : "", Locator.Value);
			_selected = new Lazy<string>(() => Do(x => x.ExecuteScript<string>(SelectScripts.GetSelectedOptionText(_selector))));
			_options =
				new Lazy<IEnumerable<string>>(
					() => Do(x => x.ExecuteScript<IList<object>>(SelectScripts.GetDropDownOptions(), _selector)).Cast<string>());
		}

		public void Select<T>(T text)
		{
			Object.WaitForPageLoading();

			Do(x => x.ExecuteScript<bool>(SelectScripts.SelectOptionByText(), _selector, text.ToString()));

			Object.WaitForCondition(x => HasSelectedItem(text), 3);
			Object.WaitForPageLoading();
		}

		public IEnumerable<string> Options
		{
			get { return _options.Value; }
		}

		public void SelectByIndex(int index)
		{
			Do(x => x.ExecuteScript<bool>(SelectScripts.SelectOptionByIndex(), _selector, index));
		}

		public string Selected
		{
			get { return _selected.Value; }
		}

		public void SelectOption<T>(T optionContent)
		{
			SelectOption(x => ComparerHelper.ContentEquals(x, optionContent.ToString()));
		}

		public void SelectOption(Func<string, bool> predicate = null)
		{
			var optionTxt = predicate == null ? Options.FirstOrDefault() : Options.FirstOrDefault(predicate);
			if (HasSelectedItem(optionTxt))
				return;

			Select(optionTxt);
		}

		public void SelectOptionUsingClicks(Func<string, bool> predicate = null)
		{
			var optionTxt = predicate == null ? Options.FirstOrDefault() : Options.FirstOrDefault(predicate);
			if (HasSelectedItem(optionTxt))
				return;

			Click();
			var optionElement = new CommonElement(new XpathLocator(string.Format(".//option[text()='{0}']", optionTxt)), this);
			optionElement.Click();

			Object.WaitForCondition(x => HasSelectedItem(optionTxt), 3);
		}

		public void SelectBySubstr<T>(T substr)
		{
			var fullText = Options.First(x => ComparerHelper.ContentContains(x, substr));
			Select(fullText);
		}

		public bool HasSelectedItem<T>(T text)
		{
			return ComparerHelper.ContentEquals(text, Selected);
		}

		private static class SelectScripts
		{
			public static string GetDropDownOptions()
			{
				return "    var list = [];" +
					   "    var opts = $(arguments[0]).find('option');" +
					   "    for (var i = 0; i < opts.length; i++) {" +
					   "        list.push(opts[i].text);" +
					   "    }" +
					   "    return list;";
			}

			public static string SelectOptionByText()
			{
				return "    var opts = $(arguments[0]).find('option');" +
					   "    for (var i = 0; i < opts.length; i++) {" +
					   "        if (opts[i].text == arguments[1]) {" +
					   "            opts[i].selected = 'selected';" +
					   "            return true;" +
					   "        }" +
					   "    }";
			}

			public static string SelectOptionByIndex()
			{
				return "$(arguments[0]).find('option').get(arguments[1]).selected='selected'; return true;";
			}

			public static string GetSelectedOptionText(string selector)
			{
				return string.Format("return $('{0} :selected').text()", selector);
			}
		}
	}
}