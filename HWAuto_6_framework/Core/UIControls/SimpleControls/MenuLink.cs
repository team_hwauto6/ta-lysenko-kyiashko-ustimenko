﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class MenuLink : Link
	{
		public MenuLink(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
		}

		public MenuLink(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
		}

		public void Navigate()
		{
			ClickAndWaitForLoading();

			var updatedMenuItem = new Link(Locator, IoCResolver.WebDriver.WebDriver);
			WaitHelper.WaitUntilElementIsActive(updatedMenuItem, WaitTimeout.Middle);
			if (!updatedMenuItem.IsActive)
			{
				ClickAndWaitForLoading();

				if (!updatedMenuItem.IsActive)
					throw new Exception("Can't select menu item: " + this);
			}
		}
	}
}
