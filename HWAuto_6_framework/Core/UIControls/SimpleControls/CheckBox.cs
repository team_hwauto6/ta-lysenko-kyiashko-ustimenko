﻿using System;
using System.Diagnostics;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class CheckBox : SelectableOption
	{
		public CheckBox(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public CheckBox(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		protected void ChangeState(bool toChecked)
		{
			if (toChecked == IsChecked) return;
			Click();
		}

		public override void Check()
		{
			ChangeState(toChecked:true);
		}
		
		public void UnCheck()
		{
			ChangeState(toChecked:false);
		}
	}
}
