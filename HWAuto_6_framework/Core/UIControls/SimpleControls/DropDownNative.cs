﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Telenor.OneScreen.UITests.Core.Extensions;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class DropDownNative : CommonElement, ISelectable
	{
		public DropDownNative(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public DropDownNative(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public void Select<T>(T text)
		{
			Do(x => new SelectElement(x).SelectByText(text.ToString()));
			Object.WaitForCondition(x => HasSelectedOption(text));
		}

		/// <summary>
		/// Rewritten spike to make webdriver with OSE dropdown control
		/// </summary>
		public override IWebElement Object
		{
			get
			{
				Parent.ExecuteScript<bool>(string.Format("$('{0}').css('opacity', 1);", Locator.Value));
				new CommonElement(new XpathLocator(string.Format("//*[@id='{0}']/..", Locator.Value.Substring(1))), Parent).Click();

				return Object;
			}
		}

		public void SelectByValue<T>(T value)
		{
			Do(x => new SelectElement(x).SelectByValue(value.ToString()));
			Object.WaitForCondition(x => HasSelectedOption(value));
		}

		public List<DropDownOption> Options
		{
			get
			{
				return Do(x => new SelectElement(x).Options.Select(y => new DropDownOption(new XpathLocator("."), y)).ToList());
			}
		}

		public bool HasSelectedOption<T>(T text)
		{
			return ComparerHelper.ContentContains(SelectedOptionText, text) || ComparerHelper.ContentContains(SelectedOption.GetAttribute("value"), text);
		}

		public DropDownOption SelectedOption
		{
			get
			{
				var selectedOption =  Do(x => new SelectElement(x).SelectedOption);
				return new DropDownOption(new XpathLocator("."), selectedOption);
			}
		}

		public string SelectedOptionText
		{
			get { return SelectedOption.InnerText; }
		}

		public void SelectBySubstr<T>(T substr)
		{
			var optionInnerText = Options.Select(x => x.InnerText).FirstOrDefault(o => ComparerHelper.ContentContains(o, substr));
			Select(optionInnerText);
		}

		public void SelectByIndex(int index)
		{
			var optionInnerText = Options.ElementAt(index).InnerText;
			Select(optionInnerText);
		}
	}
}
