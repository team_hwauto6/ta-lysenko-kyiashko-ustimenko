﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;
using Telenor.OneScreen.UITests.Core.UIControls.Locators.OseLocators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class Message : CommonElement
	{
		public Message(ILocator locator, ISearchContext parent, int? waitTimeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, waitTimeout, waitCondition)
		{
		}

		public Message(ILocator locator, IBaseElement parent, int? waitTimeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, waitTimeout, waitCondition)
		{
		}

		public Button CloseButton => new Button(new OseCssLocator(UITests.Locators.Locators.Common.CloseMessageBtn), this);
	}
}
