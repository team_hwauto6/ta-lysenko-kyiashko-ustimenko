﻿using System;
using System.Diagnostics;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls.Locators;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Core.UIControls.SimpleControls
{
	public class InputField : Label
	{
		public InputField(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitConditon = null) : base(locator, parent, timeout, waitCondition: waitConditon)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public InputField(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitConditon = null) : base(locator, parent, timeout, waitCondition: waitConditon)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public void Clear()
		{
			if (IsPresent) Do(x => x.Clear());
		}

		public void PressEnter()
		{
			if (IsPresent)
			{
				Do(x => x.SendKeys(Keys.Enter));
			}
		}

		public void Type<T>(T textInput, bool append = false)
		{
			if (!append) Clear();
			if (!IsClickable)
			{
				throw new InvalidOperationException($"{ElementName} is not clickable");
			}
			Do(x => x.SendKeys(textInput.ToString()));
		}

		public void AppendText<T>(T text)
		{
			Type(text, true);
		}

		public InputField WaitUntilIsClickable(int waitTimeOut = WaitTimeout.Middle)
		{
			var result = WaitHelper.WaitUntilElementIsClickable(this, waitTimeOut);

			if (!result)
				throw new LoadableComponentException("Input is not clickable: " + Locator);

			return this;
		}
	}
}
