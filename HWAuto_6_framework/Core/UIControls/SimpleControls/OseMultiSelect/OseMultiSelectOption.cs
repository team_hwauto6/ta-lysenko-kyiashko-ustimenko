﻿using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls.OseMultiSelect
{
	public class OseMultiSelectOption : OseSelectableOption<CheckBox>
	{
		public OseMultiSelectOption(ILocator innerCheckBoxLocator, ILocator innerLabelLocator, ISearchContext parent, int? timeout = null)
			: base(innerLabelLocator, parent, timeout)
		{
			_innerSelectableOption = new CheckBox(innerCheckBoxLocator, parent);
		}

		public OseMultiSelectOption(ILocator innerCheckBoxLocator, ILocator innerLabelLocator, IBaseElement parent, int? timeout = null)
			: base(innerLabelLocator, parent, timeout)
		{
			_innerSelectableOption = new CheckBox(innerCheckBoxLocator, parent);
		}
	}
}
