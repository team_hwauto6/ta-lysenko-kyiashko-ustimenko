﻿using System;
using System.Diagnostics;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class DropDownOption : CommonElement
	{
		public DropDownOption(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public DropDownOption(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}
	}
}
