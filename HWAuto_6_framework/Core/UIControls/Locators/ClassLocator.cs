﻿using OpenQA.Selenium;

namespace Core.UIControls.Locators
{
	public class ClassLocator : Locator
	{
		public ClassLocator(string value) : base(value)
		{
			By = By.ClassName(value);
		}
	}
}