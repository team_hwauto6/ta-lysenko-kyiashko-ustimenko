﻿using OpenQA.Selenium;

namespace Core.UIControls.Locators
{
	public class IdLocator : Locator
	{
		public IdLocator(string value) : base(value)
		{
			By = By.Id(value);
		}
	}
}