﻿using OpenQA.Selenium;

namespace Core.UIControls.Locators
{
	public abstract class Locator : ILocator
	{
		public string Value { get; set; }
		public By By { get; set; }

		protected Locator(string value)
		{
			Value = value;
		}
	}
}