﻿using OpenQA.Selenium;

namespace Core.UIControls.Locators
{
	public class XpathLocator : Locator
	{
		/// <summary>
		/// Must be specified with the reference to current node to provide search in its context.
		/// Format './/{element_matcher}' intead of '//{element_matcher}'
		/// </summary>
		/// <param name="value"></param>
		public XpathLocator(string value) : base(value)
		{
			By = By.XPath(value);
		}
	}
}